package com.example.student.pixelnetz.Bibliothek.nachrichten;

/**
 * Created by Student on 31.08.2017.
 */

public abstract class AbstrakteSteuerungsNachricht {
    public abstract void setStartZeit(long startZeit);
    public abstract long getStartZeit();
    public abstract void setEnumAnzeigeStrategie(EnumAnzeigeStrategie enumAnzeigeStrategie);
    public abstract EnumAnzeigeStrategie getEnumAnzeigeStrategie();
}
