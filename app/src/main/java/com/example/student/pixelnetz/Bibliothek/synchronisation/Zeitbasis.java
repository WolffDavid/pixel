package com.example.student.pixelnetz.Bibliothek.synchronisation;




import android.os.AsyncTask;

import java.util.Observable;

/**
 * Created by Student on 28.05.2017.
 */

public class Zeitbasis extends Observable implements Runnable{

    private Thread thread;
    private boolean wartet;
    NetworkTimeProtocol networkTimeProtocol;

    public Zeitbasis() {
        thread = null;
        wartet = true;
    }

    public void anfordern() {
        networkTimeProtocol = (NetworkTimeProtocol) new NetworkTimeProtocol().executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);
    }

    public long getAktuelleUhrzeit() {
        return networkTimeProtocol.getAktuelleUhrzeit();
    }

    @Override
    public void run(){

        for(;;) {

            // Do stuff: Call of state machine



            // Set thread in wartet mode
            if (wartet) {
                try {
                    doWait();
                } catch (InterruptedException ex) {
                }
            }
            setChanged();
            notifyObservers();
            // Set Thread in sleep mode
            try {
                thread.sleep(1000);
            } catch (InterruptedException e) {
                e.printStackTrace();

            }
        }
    }

    public synchronized void startThread() {

        if (thread == null) {
            thread = new Thread(this);
            thread.start();
        } else {
            notifyAll();
        }
        wartet = false;
    }

    private synchronized void doWait() throws InterruptedException {

        wait();
    }

    public void waitThread() {

        wartet = true;
    }
}