package com.example.student.pixelnetz.Pixel.anzeigeStrategie;


import com.example.student.pixelnetz.Bibliothek.nachrichten.SteuerungsNachrichtPixel;
import com.example.student.pixelnetz.Bibliothek.synchronisation.Zeitbasis;
import com.example.student.pixelnetz.Pixel.Pixel;

/**
 * Created by Student on 11.09.2017.
 */

public class Blinken implements InterfaceAnzeigeStrategie,Runnable
{
    Pixel view;
    private Thread thread;
    private boolean wartet;

    private Zeitbasis zeitbasis;
    private SteuerungsNachrichtPixel nachricht;

    public Blinken(Pixel view, Zeitbasis zeitbasis) {
        this.view = view;

        this.zeitbasis = zeitbasis;
        thread = null;
        wartet = true;
    }
    @Override
    public void run(SteuerungsNachrichtPixel nachricht) {

        this.nachricht = nachricht;
        startThread();


    }
    @Override
    public void stop() {
        waitThread();
    }
    @Override
    public void run(){

        view.setDisplay(nachricht.getColor());
        long nexTime = zeitbasis.getAktuelleUhrzeit()+ nachricht.getFrequenz();


        for(;;) {

            // Do stuff: Call of state machine

            if (nexTime <= zeitbasis.getAktuelleUhrzeit()) {
                nexTime = zeitbasis.getAktuelleUhrzeit()+ nachricht.getFrequenz();
                view.toggleDisplay(nachricht.getColor());
            }

            // Set thread in wartet mode
            if (wartet) {
                try {
                    doWait();
                } catch (InterruptedException ex) {
                }
            }
        }
    }
    public synchronized void startThread() {

        if (thread == null) {
            thread = new Thread(this);
            thread.start();
        } else {
            notifyAll();
        }
        wartet = false;
    }
    private synchronized void doWait() throws InterruptedException {

        wait();
    }
    public void waitThread() {

        wartet = true;
    }
}
