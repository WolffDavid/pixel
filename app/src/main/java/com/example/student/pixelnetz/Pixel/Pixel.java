/*------------------------------------------------------------------------------------------------*/
/*                                                                                                */
/*  File:   Pixel.java                                                                            */
/*  Author: David Wolff                                                                           */
/*  Mail:   wolffda58075@th-nuernberg.de                                                          */
/*                                                                                                */
/*------------------------------------------------------------------------------------------------*/

package com.example.student.pixelnetz.Pixel;

/*------------------------------------------------------------------------------------------------*/
/*                                  Imports                                                       */
/*------------------------------------------------------------------------------------------------*/

import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.wifi.WifiInfo;
import android.net.wifi.WifiManager;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;

import org.w3c.dom.Text;

import pixelnetz.pixel.R;



/*------------------------------------------------------------------------------------------------*/
/*                                  Class Declaration                                             */
/*------------------------------------------------------------------------------------------------*/

/**
 * Created by David Wolff on 25.07.2017.
 */

public class Pixel extends AppCompatActivity implements Runnable{


    /*--------------------------------------------------------------------------------------------*/
    /*                                  Private Variable Declaration                              */
    /*--------------------------------------------------------------------------------------------*/

    private ModelPixel modelPixel;

    private boolean isDisplayAn;

    private TextView mResultTextView;

    private static final String KENNUNG_CODE = "2";

    //Netzwerk Log
    private WifiManager wifiManager;
    private ConnectivityManager connManager;
    private NetworkInfo networkInfo;
    private WifiInfo wifiInfo;
    private Button btnUpdateInfo;
    private TextView tvVerbunden;
    private TextView tvIPAdresse;
    private TextView tvNetzwerk;
    private TextView tvLinkSpeed;
    private TextView tvSubMaske;
    private TextView tvServerAdresse;
    private TextView tvGateway;

    private TextView tvSyncZeit;
    private TextView tvErrorTCP;


    /*--------------------------------------------------------------------------------------------*/
    /*                                  LifeLine Functions of activity                            */
    /*--------------------------------------------------------------------------------------------*/

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_pixel);

        Intent intent = getIntent();
        String s = intent.getStringExtra(KENNUNG_CODE);
        mResultTextView = (TextView) findViewById(R.id.result_textview);
        mResultTextView.setText(s);
        // Kennung X, Kennung Y, Anzeigenzugehörigkeit als String mit Komma getrennt übersendet
        // Muss nun aufgesplittet werden
        String[] strings;
        strings = s.split(",");
        Log.d("TEST",strings[0]);
        Log.d("TEST",strings[1]);
        Log.d("TEST",strings[2]);
        pixelkennungFestelgen(Integer.valueOf(strings[0]),Integer.valueOf(strings[1]),Integer.valueOf(strings[2]));

        setVollbildModus();

        setDisplayHelligkeit(1F);
        setDisplayPermanentAn();
        netwerkLogInit();
    }

    /*--------------------------------------------------------------------------------------------*/
    /*                                  Implemented methods to control the display                */
    /*--------------------------------------------------------------------------------------------*/

    public void wlanAktivieren(){

    }
    public void verbindungMitNetzwerkHerstellen(){

    }
    public void pixelkennungFestelgen(int x,int y, int port){
        modelPixel = new ModelPixel(port, Pixel.this);
        modelPixel.sendeKennung(x,y, port);
    }

    public void setVollbildModus(){
        getWindow().getDecorView().setSystemUiVisibility(View.SYSTEM_UI_FLAG_LAYOUT_STABLE
                | View.SYSTEM_UI_FLAG_LAYOUT_HIDE_NAVIGATION
                | View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN
                | View.SYSTEM_UI_FLAG_HIDE_NAVIGATION // hide nav bar
                | View.SYSTEM_UI_FLAG_FULLSCREEN // hide status bar
                | View.SYSTEM_UI_FLAG_IMMERSIVE
        );
    }

    public void setDisplayPermanentAn() {
        getWindow().addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);
    }
    public void setDisplayHelligkeit(float helligkeit) {
        WindowManager.LayoutParams layout = getWindow().getAttributes();
        layout.screenBrightness = helligkeit;
        getWindow().setAttributes(layout);
    }

    public void toggleDisplay(final int color) {

        if(isDisplayAn)
        {
            runOnUiThread(new Runnable() {
                @Override
                public void run()
                {
                    LinearLayout rl = (LinearLayout) findViewById(R.id.background);
                    rl.setBackgroundColor(Color.BLACK);
                }
            });
            isDisplayAn = false;
        }
        else{
            runOnUiThread(new Runnable() {
                @Override
                public void run()
                {
                    LinearLayout rl = (LinearLayout) findViewById(R.id.background);
                    rl.setBackgroundColor(color);
                }
            });
            isDisplayAn = true;
        }
    }
    public void setDisplay(final int color) {
        runOnUiThread(new Runnable()
        {
            @Override
            public void run() {
                LinearLayout rl = (LinearLayout) findViewById(R.id.background);
                rl.setBackgroundColor(color);
            }
        });
        isDisplayAn = true;
    }
    public void resetDisplay() {
        runOnUiThread(new Runnable()
        {
            @Override
            public void run() {
                LinearLayout rl = (LinearLayout) findViewById(R.id.background);
                rl.setBackgroundColor(Color.BLACK);
            }
        });
        isDisplayAn = false;
    }
    private void netwerkLogInit(){
        btnUpdateInfo = (Button)findViewById(R.id.btnUpdate);
        tvIPAdresse = (TextView)findViewById(R.id.tvIPAdresse);
        tvNetzwerk = (TextView)findViewById(R.id.tvNetzwerk);
        tvLinkSpeed = (TextView)findViewById(R.id.tvLinkSpeed);
        tvVerbunden = (TextView)findViewById(R.id.tvVerbunden);
        tvGateway = (TextView)findViewById(R.id.tvGateway);
        tvSubMaske = (TextView)findViewById(R.id.tvSubMaske);
        tvServerAdresse = (TextView)findViewById(R.id.tvServerAdresse);

        tvSyncZeit = (TextView)findViewById(R.id.tvSyncZeit);
        tvErrorTCP = (TextView)findViewById(R.id.tvError);

        btnUpdateInfo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //Todo zum Thread umbauen
                updateInfo();
                //if(running == false){
                //    startThread();
                //}else stopThread();
            }
        });
    }


    /*--------------------------------------------------------------------------------------------*/
    /*                                  Debug                                                     */
    /*--------------------------------------------------------------------------------------------*/
    private void updateInfo(){

        wifiManager = (WifiManager) this.getApplicationContext().getSystemService(Context.WIFI_SERVICE);
        connManager = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
        networkInfo = connManager.getNetworkInfo(ConnectivityManager.TYPE_WIFI);
        wifiInfo = wifiManager.getConnectionInfo();


        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                tvVerbunden.setText(networkInfo.isConnected()+"");
                tvNetzwerk.setText(wifiInfo.getSSID()+"");
                tvIPAdresse.setText(ip2String(wifiInfo.getIpAddress()));
                tvLinkSpeed.setText(wifiInfo.getLinkSpeed()+"");
                tvServerAdresse.setText(ip2String(wifiManager.getDhcpInfo().serverAddress));
                tvGateway.setText(ip2String(wifiManager.getDhcpInfo().gateway));
                tvSubMaske.setText(ip2String(wifiManager.getDhcpInfo().netmask));
            }
        });

        tvVerbunden.setText(networkInfo.isConnected()+"");
        tvNetzwerk.setText(wifiInfo.getSSID()+"");
        tvIPAdresse.setText(ip2String(wifiInfo.getIpAddress()));
        tvLinkSpeed.setText(wifiInfo.getLinkSpeed()+"");
        tvServerAdresse.setText(ip2String(wifiManager.getDhcpInfo().serverAddress));
        tvGateway.setText(ip2String(wifiManager.getDhcpInfo().gateway));
        tvSubMaske.setText(ip2String(wifiManager.getDhcpInfo().netmask));

        tvErrorTCP.setText(modelPixel.getTcpClient().errorMsg);
        tvSyncZeit.setText(modelPixel.getZeitbasis().getAktuelleUhrzeit()+"");


        Log.i("WfInfo.getIPAddress",wifiInfo.getIpAddress()+"");
        Log.i("WfInfo.getSSID",wifiInfo.getSSID()+"");
        Log.i("WfInfo.getNetworkId",wifiInfo.getNetworkId()+"");
        Log.i("WfInfo.getLinkSpeed",wifiInfo.getLinkSpeed()+"");
        Log.i("NetworkInfo.isConnected",networkInfo.isConnected()+"");
        Log.i("DhcpInfo.netmask", ip2String(wifiManager.getDhcpInfo().netmask));
        Log.i("DhcpInfo.gateway", ip2String(wifiManager.getDhcpInfo().gateway));
        Log.i("DhcpInfo.ServerAdresse", ip2String(wifiManager.getDhcpInfo().serverAddress));
    }
    private String ip2String(int ip){
        String string = String.format("%d.%d.%d.%d", (ip & 0xff),
                (ip >> 8 & 0xff), (ip >> 16 & 0xff), (ip >> 24 & 0xff));
        return string;
    }

    private boolean running = false;
    private Thread updateThread;
    private void startThread(){
        running = true;
        updateThread = new Thread(this);
        updateThread.setName("UpdateInfo");
        updateThread.start();
    }

    private void stopThread(){
        running = false;
        while (true){
            try{
                updateThread.join();
                break;
            }catch (InterruptedException e){
                tvErrorTCP.setText(e.toString());
                e.printStackTrace();
            }
        }
    }
    @Override
    public void run() {
        while (running) {
            try {
                Thread.sleep(1000);
                updateInfo();
            }catch (InterruptedException e){
                tvErrorTCP.setText(e.toString());
                e.printStackTrace();
            }
        }

    }

/*------------------------------------------------------------------------------------------------*/
/*                                  End of class                                                  */
/*------------------------------------------------------------------------------------------------*/
}
