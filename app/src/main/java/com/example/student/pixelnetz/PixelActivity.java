package com.example.student.pixelnetz;


import android.Manifest;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Point;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.wifi.WifiInfo;
import android.net.wifi.WifiManager;
import android.os.Bundle;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;


import com.google.android.gms.common.api.CommonStatusCodes;
import com.google.android.gms.vision.barcode.Barcode;


import com.example.student.pixelnetz.Bibliothek.verbindung.Netzwerk;
import com.example.student.pixelnetz.Pixel.Pixel;

import pixelnetz.pixel.R;

import com.example.student.pixelnetz.barcode.BarcodeCaptureActivity;


public class PixelActivity extends AppCompatActivity {

    private final String NETZWERKNAME = "DeathNoteSpot";
    private final String NETZPASSWORT = "sehtmichan";

    //Netzwerk
    private Netzwerk netzwerk;
    private WifiManager wifiManager;
    private ConnectivityManager connManager;
    private NetworkInfo networkInfo;
    private WifiInfo wifiInfo;

    private Button btnUpdateInfo;
    private TextView tvVerbunden;
    private TextView tvIPAdresse;
    private TextView tvNetzwerk;
    private TextView tvLinkSpeed;
    private TextView tvServerAdresse;
    private TextView tvGateway;
    private TextView tvSubMaske;

    //QR-Scan
    private Button scanBarcodeButton;
    //manuelleEingabe
    private Button manuelleEingabe;
    private TextView tv_port;
    private EditText et_posX;
    private EditText et_posY;



    private static final String LOG_TAG = PixelActivity.class.getSimpleName();
    private static final int BARCODE_READER_REQUEST_CODE = 1;
    private static final String KENNUNG_CODE = "2";

    private TextView mResultTextView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        //Init View
        initView();
        initActions();

        initCustonExceptionHandler();
        netzwerkInit();
        updateInfo();


        //Log.i("getConnectionInfo",wifiManager.g);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == BARCODE_READER_REQUEST_CODE) {
            if (resultCode == CommonStatusCodes.SUCCESS) {
                if (data != null) {
                    Barcode barcode = data.getParcelableExtra(BarcodeCaptureActivity.BarcodeObject);
                    Point[] p = barcode.cornerPoints;
                    mResultTextView.setText(barcode.displayValue);

                    Intent intent = new Intent(this,Pixel.class);
                    intent.putExtra(KENNUNG_CODE,String.valueOf(barcode.displayValue));
                    startActivity(intent);

                } else mResultTextView.setText(R.string.no_barcode_captured);
            } else Log.e(LOG_TAG, String.format(getString(R.string.barcode_error_format),
                    CommonStatusCodes.getStatusCodeString(resultCode)));
        } else super.onActivityResult(requestCode, resultCode, data);
    }
    /*--------------------------------------------------------------------------------------------*/
    /*                                  View Init                                                 */
    /*--------------------------------------------------------------------------------------------*/

    private void initView() {
        mResultTextView = (TextView) findViewById(R.id.result_textview);
        scanBarcodeButton = (Button) findViewById(R.id.scan_barcode_button);
        manuelleEingabe= (Button)findViewById(R.id.btn_manuelleEingabe);
        et_posX = (EditText)findViewById(R.id.et_posX);
        et_posY = (EditText)findViewById(R.id.et_posY);
        tv_port = (TextView)findViewById(R.id.tv_port);

        btnUpdateInfo = (Button)findViewById(R.id.btnUpdate);
        tvIPAdresse = (TextView)findViewById(R.id.tvIPAdresse);
        tvNetzwerk = (TextView)findViewById(R.id.tvNetzwerk);
        tvLinkSpeed = (TextView)findViewById(R.id.tvLinkSpeed);
        tvVerbunden = (TextView)findViewById(R.id.tvVerbunden);
        tvSubMaske = (TextView)findViewById(R.id.tvSubMaske);
        tvServerAdresse = (TextView)findViewById(R.id.tvServerAdresse);
        tvGateway = (TextView)findViewById(R.id.tvGateway);
    }

    /*--------------------------------------------------------------------------------------------*/
    /*                                  Controller Init                                           */
    /*--------------------------------------------------------------------------------------------*/

    private void initActions() {

        scanBarcodeButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(getApplicationContext(), BarcodeCaptureActivity.class);
                startActivityForResult(intent, BARCODE_READER_REQUEST_CODE);
            }
        });

        manuelleEingabe.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String string = (et_posX.getText().toString()+","+et_posY.getText().toString()+","+tv_port.getText().toString());
                Intent intent = new Intent(getApplicationContext(),Pixel.class);
                intent.putExtra(KENNUNG_CODE,string);
                startActivity(intent);
            }
        });

        btnUpdateInfo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                updateInfo();
            }
        });
    }

    private void netzwerkInit(){
        wifiManager = (WifiManager) this.getApplicationContext().getSystemService(Context.WIFI_SERVICE);
        netzwerk = new Netzwerk(wifiManager);
        netzwerk.verbinden(NETZWERKNAME,NETZPASSWORT);
        while(netzwerk.isVerbindungAktiv());
    }

    /*--------------------------------------------------------------------------------------------*/
    /*                                  Debug                                                     */
    /*--------------------------------------------------------------------------------------------*/
    private void updateInfo(){

        wifiManager = (WifiManager) this.getApplicationContext().getSystemService(Context.WIFI_SERVICE);
        connManager = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
        networkInfo = connManager.getNetworkInfo(ConnectivityManager.TYPE_WIFI);
        wifiInfo = wifiManager.getConnectionInfo();

        tvVerbunden.setText(networkInfo.isConnected()+"");
        tvNetzwerk.setText(wifiInfo.getSSID()+"");
        tvIPAdresse.setText(ip2String(wifiInfo.getIpAddress()));
        tvLinkSpeed.setText(wifiInfo.getLinkSpeed()+"");
        tvServerAdresse.setText(ip2String(wifiManager.getDhcpInfo().serverAddress));
        tvGateway.setText(ip2String(wifiManager.getDhcpInfo().gateway));
        tvSubMaske.setText(ip2String(wifiManager.getDhcpInfo().netmask));
    }
    private String ip2String(int ip){
        String string = String.format("%d.%d.%d.%d", (ip & 0xff),
                (ip >> 8 & 0xff), (ip >> 16 & 0xff), (ip >> 24 & 0xff));
        return string;
    }

    /*--------------------------------------------------------------------------------------------*/
    /*                                 ExceptionHandler                                           */
    /*--------------------------------------------------------------------------------------------*/
    public final int MY_PERMISSIONS_REQUEST_WRITE_STORAGE = 134;


    private void initCustonExceptionHandler(){
        holeErlaubnisAufZugriff();
        Thread.setDefaultUncaughtExceptionHandler(new CustomizedExceptionHandler(
                "/mnt/sdcard/"));
    }


    private void holeErlaubnisAufZugriff(){

        if (ContextCompat.checkSelfPermission(this,
                Manifest.permission.WRITE_EXTERNAL_STORAGE)
                != PackageManager.PERMISSION_GRANTED) {
            // Should we show an explanation?
            if (ActivityCompat.shouldShowRequestPermissionRationale(this,
                    Manifest.permission.WRITE_EXTERNAL_STORAGE)) {
                // Show an explanation to the user *asynchronously* -- don't block
                // this thread waiting for the user's response! After the user
                // sees the explanation, try again to request the permission.
            } else {

                // No explanation needed, we can request the permission.

                ActivityCompat.requestPermissions(this,
                        new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE},
                        MY_PERMISSIONS_REQUEST_WRITE_STORAGE);

                // MY_PERMISSIONS_REQUEST_READ_CONTACTS is an
                // app-defined int constant. The callback method gets the
                // result of the request.
            }
            Log.i("holeErlaubnisAufZugriff","Zugriff verweigert");
            //return false;
        }
        Log.i("holeErlaubnisAufZugriff","Zugriff genehmigt");
        //return true;
    }
    @Override
    public void onRequestPermissionsResult(int requestCode, String permissions[],
                                           int[] grantResults) {
        Log.i(getClass().getSimpleName(), "onRequestPermissionsResult()");
        switch (requestCode) {
            case MY_PERMISSIONS_REQUEST_WRITE_STORAGE:
                // If request is cancelled, the result arrays are empty.
                if (grantResults.length > 0
                        && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    Log.i(getLocalClassName(), "onRequestPermissionsResult GRANTED, NOW SCREW AROUND");
                    // permission was granted, yay! Do the
                    // contacts-related task you need to do.

                } else {
                    Log.i(getLocalClassName(), "onRequestPermissionsResult DENIEEEED");

                    //exit application
                    this.finishAffinity();
                    // permission denied, boo! Disable the
                    // functionality that depends on this permission.
                }
                return;
            // other 'case' lines to check for other
            // permissions this app might request
        }
    }
}

