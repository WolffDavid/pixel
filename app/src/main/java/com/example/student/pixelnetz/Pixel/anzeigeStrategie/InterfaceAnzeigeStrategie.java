package com.example.student.pixelnetz.Pixel.anzeigeStrategie;


import com.example.student.pixelnetz.Bibliothek.nachrichten.SteuerungsNachrichtPixel;

/**
 * Created by Student on 11.09.2017.
 */

public interface InterfaceAnzeigeStrategie
{
    void run(SteuerungsNachrichtPixel nachricht);

    void stop();
}
