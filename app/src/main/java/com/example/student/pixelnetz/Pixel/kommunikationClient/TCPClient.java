package com.example.student.pixelnetz.Pixel.kommunikationClient;

/**
 * Created by Student on 31.08.2017.
 */

import android.os.AsyncTask;
import android.util.Log;



import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.net.Socket;
import java.net.SocketException;

import com.example.student.pixelnetz.Bibliothek.nachrichten.AbstrakteSteuerungsNachricht;
import com.example.student.pixelnetz.Bibliothek.nachrichten.RegistrierungsNachricht;
import com.example.student.pixelnetz.Bibliothek.nachrichten.SteuerungsNachrichtPixel;
import com.example.student.pixelnetz.Pixel.ModelPixel;

/**
 * Created by Student on 02.08.2017.
 */

public class TCPClient extends AsyncTask<RegistrierungsNachricht,SteuerungsNachrichtPixel,Boolean>
{
    public String errorMsg = "";

    /***********************************************************************************************/
    /*                                  Private Variable Declaration                               */
    /***********************************************************************************************/

    private String serverSocketIp = "192.168.1.132";
    private int port;
    private Socket socket = null;

    private boolean verbunden = false;
    private ModelPixel modelPixel;
    private AbstrakteSteuerungsNachricht steuerungsNachricht;

    /***********************************************************************************************/
    /*                                  Constructor                                                */
    /***********************************************************************************************/

    public TCPClient(ModelPixel modelPixel, int port) {
        this.port = port;
        this.modelPixel = modelPixel;
    }

    /***********************************************************************************************/
    /*                                  Public Functions                                           */
    /***********************************************************************************************/


    /***********************************************************************************************/
    /*                                  Abstract methods of AsyncTask                              */
    /***********************************************************************************************/

    @Override
    protected Boolean doInBackground(RegistrierungsNachricht... params)
    {
        this.connect(serverSocketIp,port);

        RegistrierungsNachricht msg = params[0];
        msg.setInetAddress(socket.getLocalAddress());

        sendMessage(socket, msg);

        while(!isCancelled())
        {
            steuerungsNachricht = receiveMessage(socket);
            modelPixel.setNaechsteAnimation(steuerungsNachricht);
        }
        return null;
    }

    /***********************************************************************************************/
    /*                                  Private Functions                                          */
    /***********************************************************************************************/

    public void connect(String serverSocketIp, int port) {

        while (!verbunden) {
            try {
                socket = new Socket(serverSocketIp, port);
                Log.i(String.valueOf(this.getClass()),"Connected");
                verbunden = true;

            } catch (SocketException se) {
                errorMsg = se.toString();
                se.printStackTrace();

            } catch (IOException e) {
                errorMsg = e.toString();
                e.printStackTrace();
            }
        }
    }

    private void sendMessage(Socket socket, RegistrierungsNachricht nachricht)
    {
        ObjectOutputStream outputStream = null;
        try
        {
            outputStream = new ObjectOutputStream(socket.getOutputStream());
        } catch (IOException e)
        {
            e.printStackTrace();
        }

        Log.i(String.valueOf(this.getClass()),"Object to be written = " + nachricht);
        try
        {
            outputStream.writeObject(nachricht);
        } catch (IOException e)
        {
            e.printStackTrace();
        }
    }

    private AbstrakteSteuerungsNachricht receiveMessage(Socket socket)
    {
        ObjectInputStream inStream = null;
        AbstrakteSteuerungsNachricht cntMsg = null;

        Log.i(String.valueOf(this.getClass()), "receive");
        try
        {
            inStream = new ObjectInputStream(socket.getInputStream());
        } catch (IOException e)
        {
            e.printStackTrace();
        }
        Log.i(String.valueOf(this.getClass()), "receive");

        try
        {
            cntMsg = (AbstrakteSteuerungsNachricht) inStream.readObject();
        } catch (IOException e)
        {
            e.printStackTrace();
        } catch (ClassNotFoundException e)
        {
            e.printStackTrace();
        }
        return cntMsg;
    }

/**************************************************************************************************/
/*                                  End of class                                                  */
/**************************************************************************************************/
}
