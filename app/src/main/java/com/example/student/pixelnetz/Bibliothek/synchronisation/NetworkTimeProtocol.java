package com.example.student.pixelnetz.Bibliothek.synchronisation;

import android.os.AsyncTask;
import android.os.SystemClock;
import android.util.Log;

/**
 * Created by Student on 16.06.2017.
 */

public class NetworkTimeProtocol extends AsyncTask<String,String,Boolean>{

    long anfangsZeitpunkt;
    SntpClient sntpClient;

    public NetworkTimeProtocol() {
        Log.i("NetworkTimeProtocol", "Constructor called");
        sntpClient = new SntpClient();

    }

    @Override
    protected Boolean doInBackground(String... params) {


        Log.i("NetworkTimeProtocol", "doInBackground started");

       //while (!isCancelled()) {
            //anfordern();
        //}
        anfordern();

        return null;
    }

    @Override
    protected void onProgressUpdate(String... progress) {
        super.onProgressUpdate(progress);
        String temp = progress[0].trim();
    }

    private void anfordern() {

        if (sntpClient.requestTime("192.168.1.1", 30000)) {
            anfangsZeitpunkt = sntpClient.getNtpTime();

            Log.i("NTP tag", String.valueOf(anfangsZeitpunkt));
        }

    }

    public long getAktuelleUhrzeit() {
        return anfangsZeitpunkt + SystemClock.elapsedRealtime() -
                sntpClient.getNtpTimeReference();
    }

}
