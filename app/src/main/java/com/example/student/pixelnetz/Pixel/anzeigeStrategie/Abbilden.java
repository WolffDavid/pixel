package com.example.student.pixelnetz.Pixel.anzeigeStrategie;


import com.example.student.pixelnetz.Bibliothek.nachrichten.SteuerungsNachrichtPixel;
import com.example.student.pixelnetz.Pixel.Pixel;

/**
 * Created by Student on 11.09.2017.
 */

public class Abbilden implements InterfaceAnzeigeStrategie {

    Pixel view;

    public Abbilden(Pixel view)
    {
        this.view = view;
    }

    @Override
    public void run(SteuerungsNachrichtPixel nachricht) {

        view.setDisplay(nachricht.getColor());

    }

    @Override
    public void stop() {

    }
}
