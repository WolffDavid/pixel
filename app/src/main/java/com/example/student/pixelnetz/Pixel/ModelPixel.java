/*------------------------------------------------------------------------------------------------*/
/*                                                                                                */
/*  File:   ModelPixel.java                                                                       */
/*  Author: David Wolff                                                                           */
/*  Mail:   wolffda58075@th-nuernberg.de                                                          */
/*                                                                                                */
/*------------------------------------------------------------------------------------------------*/

package com.example.student.pixelnetz.Pixel;

/*------------------------------------------------------------------------------------------------*/
/*                                  Imports                                                       */
/*------------------------------------------------------------------------------------------------*/

import android.os.AsyncTask;
import android.util.Log;

import java.util.Observable;
import java.util.Observer;
import java.util.Timer;
import java.util.TimerTask;

import com.example.student.pixelnetz.Bibliothek.nachrichten.AbstrakteSteuerungsNachricht;
import com.example.student.pixelnetz.Bibliothek.nachrichten.RegistrierungsNachricht;
import com.example.student.pixelnetz.Bibliothek.nachrichten.SteuerungsNachrichtPixel;
import com.example.student.pixelnetz.Bibliothek.synchronisation.Zeitbasis;
import com.example.student.pixelnetz.Pixel.anzeigeStrategie.Abbilden;
import com.example.student.pixelnetz.Pixel.anzeigeStrategie.Blinken;
import com.example.student.pixelnetz.Pixel.anzeigeStrategie.InterfaceAnzeigeStrategie;
import com.example.student.pixelnetz.Pixel.kommunikationClient.TCPClient;

/*------------------------------------------------------------------------------------------------*/
/*                                  Class Declaration                                             */
/*------------------------------------------------------------------------------------------------*/

/**
 * Created by Student on 31.08.2017.
 */

public class ModelPixel implements Observer {

    /*--------------------------------------------------------------------------------------------*/
    /*                                  Private Variable Declaration                              */
    /*--------------------------------------------------------------------------------------------*/

    public Zeitbasis getZeitbasis() {
        return zeitbasis;
    }

    public TCPClient getTcpClient() {
        return tcpClient;
    }

    private Zeitbasis zeitbasis;
    private TCPClient tcpClient;

    private InterfaceAnzeigeStrategie anzeigeStrategie;
    private Pixel view;
    private Timer timer;

    /*--------------------------------------------------------------------------------------------*/
    /*                                  Constructor                                               */
    /*--------------------------------------------------------------------------------------------*/

    /**
     * @param port  Each Anzeige has one port. If an Anzeige has an port of 10000 the Pixel which
     *              gives this number to the constructor belongs to this Anzeige.
     * @param view  The view is used to call the display control methods of the Pixel class.
     */
    public ModelPixel(int port , Pixel view) {

        this.view = view;
        timer = new Timer();

        synchronisationsAnfrageSenden();

        tcpClient = new TCPClient(this,port);
    }

    /*--------------------------------------------------------------------------------------------*/
    /*                                  Implemented methods to use before animation start         */
    /*--------------------------------------------------------------------------------------------*/

    /**
     * @param position  If a QR code was scanned the Pixel gets information about its position in
     *                  an Anzeige. For example: 11 Means the position (1/1)
     * @param anzeige   The QR code includes also the information which Anzeige the Pixel belongs
     *                  to. The information is the portnumber of Anzeige.
     */
    public void sendeKennung(int x, int y, int anzeige) {
        //Execute on executer da nur ein thread geht
        RegistrierungsNachricht msg = new RegistrierungsNachricht();
        msg.setKennung(x,y);
        msg.setPort(anzeige);
        tcpClient.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR,msg);
    }

    /**
     * @param nachricht The pixel gets the Steuerungsnachrichten in close succession.
     *                  Therefore the Timer class of AndroidApi is used. The timer starts a
     *                  scheduler which triggers the animation at given timestamp.
     */
    public void setNaechsteAnimation(AbstrakteSteuerungsNachricht nachricht){
        final SteuerungsNachrichtPixel cntMsg = (SteuerungsNachrichtPixel) nachricht;
        switch(cntMsg.getEnumAnzeigeStrategie())
        {
            case STOP:
                //
                timer.cancel();
                starteAnimation(cntMsg);
                timer = new Timer();
                break;
            default:
                timer.schedule(new TimerTask() {
                    @Override
                    public void run() {
                        starteAnimation(cntMsg);
                    }
                },cntMsg.getStartZeit()-this.zeitbasis.getAktuelleUhrzeit());
                break;
        };
    }

    /*--------------------------------------------------------------------------------------------*/
    /*                                  Implemented methods to during animation                   */
    /*--------------------------------------------------------------------------------------------*/

    /**
     * @param arg   The Timer scheduler starts this function at a given timestamp. This function
     *              makes a decision wheter the next animation is blinken, abbilden or stop.
     *              After that it calls setDisplayStrategy with the animation to start.
     */
    private void starteAnimation(Object arg) {
        if(anzeigeStrategie != null)
        {
            anzeigeStrategie.stop();
        }

        SteuerungsNachrichtPixel msg = (SteuerungsNachrichtPixel)arg;
        switch(msg.getEnumAnzeigeStrategie())
        {
            case ABBILDEN:
                this.setDisplayStrategy(new Abbilden(view));

                break;

            case STOP:
                this.setDisplayStrategy(new Abbilden(view));

                break;

            case BLINKEN:
                this.setDisplayStrategy(new Blinken(view, zeitbasis));

                break;
        }

        anzeigeStrategie.run((SteuerungsNachrichtPixel)arg);
    }

    /**
     * @param strategy  This is the displayStrategy which the display of the mobilephone should
     *                  display.
     */
    private void setDisplayStrategy(InterfaceAnzeigeStrategie strategy) {
        this.anzeigeStrategie = strategy;
    }

    public void synchronisationsAnfrageSenden(){
        zeitbasis = new Zeitbasis();
        zeitbasis.addObserver(this);
        zeitbasis.anfordern();
    }

    /*--------------------------------------------------------------------------------------------*/
    /*                                  Implemented method of Observer                            */
    /*--------------------------------------------------------------------------------------------*/

    /**
     * @param o     This class is an observer of the Zeitbasis to check the wright zeitbasis.
     * @param arg   This value isn't used.
     */
    @Override
    public void update(Observable o, final Object arg) {
        if(o instanceof Zeitbasis)
        {
            Log.i("NTP tag2", String.valueOf(zeitbasis.getAktuelleUhrzeit()));
        }
    }
}
